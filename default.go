package csid

import (
	"math/rand"
	"time"
)

// Epoch is the Epoch for the default generator
// set to 2022-02-22 22:22:22 UTC aka "twosday"
var Epoch = time.Unix(1645568542, 0)

// Epoch2000 is an alternate default epoch
// set to 2000-01-01 00:00:00 UTC for a more typical/practical date
// requires setting up your own Generator object to use it though
var Epoch2000 = time.Unix(978220800, 0)

// 2022-01-01 00:00:00 UTC
//var Epoch = time.Unix(1640995200, 0)

// Default is a default Generator, useful for low volume projects
// for higher volumes its recommended to create one yourself however
var Default = Generator{
	Epoch: Epoch,
	epoch: Epoch.UnixMilli(),

	// Starting things off at random points to reduce the risk of conflict
	// at least for the default version which is probably going to be low volume
	// bigger applications or ones that scale horzontally may want to do their own config
	Worker:    rand.New(rand.NewSource(time.Now().UnixNano())).Uint64() & MaxWorker,
	increment: rand.New(rand.NewSource(time.Now().Unix())).Uint64(),
}

// Get a new ID from the default generator
func Get() ID {
	return Default.Get()
}

// IDTime returns the time of an ID relative to the default Epoch
func IDTime(i ID) time.Time {
	return i.Time(Epoch)
}
