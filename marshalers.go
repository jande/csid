package csid

import (
	"encoding/binary"
	"errors"
)

func (i ID) MarshalJSON() ([]byte, error) {
	return []byte("\"" + i.String() + "\""), nil
}

func (i *ID) UnmarshalJSON(b []byte) error {
	n, err := parseJSON(string(b))
	if err != nil {
		return err
	}
	*i = ID(n)
	return nil
}

func (i ID) MarshalText() ([]byte, error) {
	return []byte(i.String()), nil
}

func (i *ID) UnmarshalText(b []byte) error {
	n, err := Parse(string(b))
	if err != nil {
		return err
	}
	*i = ID(n)
	return nil
}

func (i ID) MarshalBinary() ([]byte, error) {
	var b = make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(i))
	return b, nil
}

func (i *ID) UnmarshalBinary(b []byte) error {
	if len(b) != 8 {
		return errors.New("invalid length for CSID")
	}
	*i = ID(binary.BigEndian.Uint64(b))
	return nil
}
