# csid
An ID generator losely derived from the snowflake format used by twitter and discord though with some differences. a CSID consists of a 42 bit timestamp measured in centseconds (equal to 10ms, rather than miliseconds to allow for lower volume more long term use) a 10 bit process identifier, and a 12 bit increment. and CSIDs are canonically displayed in Base16 rather than Base10.

### Traits
- Lasts over 1000 years before the timestamp runs out
- Dependency free
- At scale some setup may be required for the machine ID (but at small scales it can be randomly set)
- Code generator for statically typed IDs
- Lockless and Concurrency Safe