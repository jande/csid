package csid

import (
	"math"
	"math/rand"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

const CentiSecond = time.Millisecond * 10

const maxIncr = 0xFFF
const MaxWorker = 0b11111_11111

// Parse parses a CSID from a String
func Parse(id string) (ID, error) {
	i, err := strconv.ParseUint(id, 16, 64)
	return ID(i), err
}

// parseJSON behaves like parse, except it distinguishes between a quoted or unquoted input
// a string from say a json parser will normally be quoted, but if its not then its a json number
// and a json number will likely be base10 rather than base16
func parseJSON(id string) (ID, error) {
	nid := strings.Trim(id, "\"")
	base := 16
	if nid == id {
		// if the strings stay the same length that means no quotes were removed
		base = 10
	}

	i, err := strconv.ParseUint(nid, base, 64)
	return ID(i), err
}

// ID is a CSID, which is akin to a snowflake as seen in twitter or discord
// but slightly different.
// instead of miliseconds the timestamp is stored in centiseconds (10ms) giving
// the ID format a much longer lifespan (over 1000 years) and to differentiate
// itself visually is canonically formatted in hexadecimal instead of decimal
type ID uint64

// IsZero returns true if an ID is blank (0)
func (i ID) IsZero() bool { return i == 0 }

// RelativeTime returns a time.RelativeTime of the time in an ID,
// note with timestamps over 270 years from the epoch it will overflow and not return correctly
func (i ID) RelativeTime() time.Duration {
	t := i >> 22
	return time.Duration(t) * CentiSecond
}

// Time returns the time of the ID relative to an epoch
func (i ID) Time(epoch time.Time) time.Time {
	t := time.Duration(i >> 22)
	if t < (math.MaxUint64 / 100) {
		return epoch.Add(t * CentiSecond)
	}

	at := epoch
	t /= 10
	for x := 0; x < 10; x++ {
		at = at.Add(t * CentiSecond)
	}
	return at
}

func (i ID) String() string {
	return strconv.FormatUint(uint64(i), 16)
}

func New(worker uint64, epoch time.Time) *Generator {
	if worker&^MaxWorker != 0 {
		panic("invalid worker code, worker must fit within a 10 bit unsigned integer")
	}

	return &Generator{
		Epoch:     epoch,
		epoch:     epoch.UnixMilli(),
		Worker:    worker,
		increment: rand.Uint64(),
	}
}

// Generator contains some settings for id generation
type Generator struct {
	Epoch  time.Time
	epoch  int64
	Worker uint64

	increment uint64
}

// Get returns a new ID
func (g *Generator) Get() ID {
	var id uint64

	t := (time.Now().UnixMilli() - g.epoch) / 10
	id |= uint64(t) << 22
	id |= g.Worker << 12
	id |= g.newIncrement()

	return ID(id)
}

type RelativeTime interface {
	Time(epoch time.Time) time.Time
}

// IDTime gets the time of an ID relative to this generators epoch
func (g *Generator) IDTime(i RelativeTime) time.Time {
	return i.Time(g.Epoch)
}

func (g *Generator) newIncrement() uint64 {
	i := atomic.AddUint64(&g.increment, 1)
	return i & maxIncr
}
